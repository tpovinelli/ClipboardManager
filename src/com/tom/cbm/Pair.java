package com.tom.cbm;

import org.jetbrains.annotations.Contract;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author Thomas Povinelli
 * Created 6/14/18
 * In ClipboardManager
 */
public class Pair<F, S> implements Serializable, Cloneable, Map.Entry<F, S> {
  public final F first;
  public final S second;

  public static <F, S extends F> Stream<F> streamSequence(Pair<F, S> pair) {
    return Stream.of(pair.first, pair.second);
  }

  public Pair(F first, S second) {
    this.first = first;
    this.second = second;
  }

  public Stream<F> streamFirst() {
    return Stream.of(first);
  }

  public Stream<S> streamSecond() {
    return Stream.of(second);
  }

  public Pair<F, S> copy() {
    return new Pair<>(this.first, this.second);
  }

  @Override
  public int hashCode() {
    return Objects.hash(first, second);
  }

  @Override
  public F getKey() {
    return first;
  }

  @Override
  public S getValue() {
    return second;
  }

  @Override
  public S setValue(S value) throws UnsupportedOperationException {
    throw new UnsupportedOperationException("com.tom.cbm.Pair is immutable and cannot be changed");
  }

  @Contract(value = "null -> false", pure = true)
  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Pair)) {
      return false;
    }
    final Pair<?, ?> pair = (Pair<?, ?>) o;
    return Objects.equals(first, pair.first) &&
        Objects.equals(second, pair.second);
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

  @Override
  public String toString() {
    return "Pair{" +
           "first=" + first +
           ", second=" + second +
           '}';
  }
}
