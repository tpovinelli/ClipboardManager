package com.tom.cbm;

import javafx.scene.input.DataFormat;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Thomas Povinelli
 * Created 2018-Feb-10
 * In ClipboardManager
 */
public class ClipboardTransaction {

  private Object data;
  private LocalDateTime time;
  private DataFormat format;
  private String plainData;

  public ClipboardTransaction(Object data, String plain, LocalDateTime time,
                              DataFormat format)
  {
    this.data = Objects.requireNonNull(data, "Cannot place null into ClipboardTransaction data");
    this.time = time;
    this.format = format;
    this.plainData = Objects.requireNonNull(plain, "Cannot place null into ClipboardTransaction plain data");
  }

  public String getPlainData() {
    return plainData;
  }

  public Object getData() {
    return data;
  }

  public LocalDateTime getTime() {
    return time;
  }

  public DataFormat getFormat() {
    return format;
  }

}
