package com.tom.cbm;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.DataFormat;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

import static tom.utils.kotlin.KotlinUtilsKt.fatalError;

/**
 * @author Thomas Povinelli
 * Created 2018-Feb-09
 * In ClipboardManager
 */
@SuppressWarnings("FieldCanBeLocal")
public class AppGUI {
  private Button updateButton = new Button("Update");
  private Button clearButton = new Button("Clear Table");
  private Button copyButton = new Button("Copy");
  private Button copyPlainButton = new Button("Copy Plain");
  private Button saveButton = new Button("Save content");
  private Button saveAsButton = new Button("Save content as...");
  private Button deleteButton = new Button("Delete");
  private Button textUpButton = new Button("Increase Text Size");
  private Button textDownButton = new Button("Decrease Text Size");
  private Button clearClipboard = new Button("Clear Clipboard");
  private Button changeRefreshDuration = new Button("Change Refresh Duration");
  private Button changeMaxMemoryButton = new Button("Set Max Memory");
  private Label memoryUsageLabel = new Label();

  private TableView<ClipboardTransaction> historyTable = new TableView<>();

  private TableColumn<ClipboardTransaction, Object> firstColumn = new TableColumn<>("Content");
  private TableColumn<ClipboardTransaction, DataFormat> secondColumn = new TableColumn<>("Type");
  private TableColumn<ClipboardTransaction, LocalDateTime> thirdColumn = new TableColumn<>("Time");

  private AppController controller;

  private HBox buttonBoxTop = new HBox();
  private HBox buttonBoxBottom = new HBox();
  private VBox mainBox = new VBox();

  private Scene scene;
  private Stage primaryStage;

  public AppGUI(Stage primaryStage) {

    this.controller = new AppController(this);
    this.primaryStage = primaryStage;
    controller.registerCloseRequest();

    firstColumn.setCellValueFactory(new PropertyValueFactory<>("data"));
    secondColumn.setCellValueFactory(new PropertyValueFactory<>("format"));
    thirdColumn.setCellValueFactory(new PropertyValueFactory<>("time"));

    firstColumn.prefWidthProperty()
               .bind(historyTable.widthProperty().divide(2.0));
    secondColumn.prefWidthProperty()
                .bind(historyTable.widthProperty().divide(4.0));
    thirdColumn.prefWidthProperty()
               .bind(historyTable.widthProperty().divide(4.0));
    //noinspection unchecked
    historyTable.getColumns()
                .addAll(firstColumn, secondColumn, thirdColumn);

    historyTable.prefHeightProperty()
                .bind(primaryStage.heightProperty().subtract(100));
    historyTable.prefWidthProperty()
                .bind(primaryStage.widthProperty().subtract(10));

    updateButton.setOnAction(e -> controller.updateDataAreaAction(true));
    clearButton.setOnAction(e -> controller.clearDataAreaAction());
    copyButton.setOnAction(e -> controller.copyItemAction());
    copyPlainButton.setOnAction(e -> controller.copyPlainItemAction());
    saveButton.setOnAction(e -> controller.saveContentAction());
    saveAsButton.setOnAction(e -> controller.saveAsContentAction());
    deleteButton.setOnAction(e -> controller.deleteItem());
    clearButton.setOnAction(e -> controller.clearDataAreaAction());
    textUpButton.setOnAction(e -> controller.increaseFontSizeAction());
    textDownButton.setOnAction(e -> controller.decreaseFontSizeAction());
    clearClipboard.setOnAction(e -> controller.clearClipboardAction());
    changeRefreshDuration.setOnAction(e -> controller.changeRefreshDurationAction());
    changeMaxMemoryButton.setOnAction(e -> controller.changeMaxMemoryAction());

    buttonBoxTop.getChildren()
                .addAll(copyButton, copyPlainButton, deleteButton, saveButton, saveAsButton,
                    clearButton, clearClipboard, updateButton);

    buttonBoxBottom.getChildren().addAll(changeRefreshDuration,
        changeMaxMemoryButton, textUpButton, textDownButton);

    buttonBoxTop.setAlignment(Pos.CENTER);
    buttonBoxBottom.setAlignment(Pos.CENTER);
    setPaddingAndSpacing(buttonBoxTop, 5.0);
    setPaddingAndSpacing(buttonBoxBottom, 5.0);
    setPaddingAndSpacing(mainBox, 5.0);

    historyTable.addEventHandler(KeyEvent.KEY_PRESSED, controller::keyPressedTableAction);
    historyTable.addEventHandler(MouseEvent.MOUSE_CLICKED, controller::mouseClickedTableAction);

    mainBox.getChildren()
           .addAll(memoryUsageLabel, historyTable, buttonBoxTop, buttonBoxBottom);
    scene = new Scene(mainBox);

    primaryStage.setWidth(Screen.getPrimary().getBounds().getWidth() * 0.9);
    primaryStage.setHeight(
        Screen.getPrimary().getBounds().getHeight() * 0.9);

    primaryStage.setScene(scene);
    primaryStage.setTitle("Clipboard Manager");
    primaryStage.show();
  }

  public static void setPaddingAndSpacing(Pane pane, double value) {
    try {
      pane.setPadding(new Insets(value));
      Method[] declaredMethods = pane.getClass().getDeclaredMethods();
      for (Method m : declaredMethods) {
        switch (m.getName()) {
          case "setVGap":
          case "setHGap":
          case "setSpacing":
            m.invoke(pane, (Object) 5.0);
            break;
        }
      }
    } catch (ReflectiveOperationException | SecurityException | IllegalArgumentException e) {
      Thread.setDefaultUncaughtExceptionHandler(AppController.oldHandler);
      fatalError(e, () -> "An unexpected error occurred in attempting to assembly the GUI");
    }
  }

  public Stage getPrimaryStage() {
    return primaryStage;
  }

  public TableView<ClipboardTransaction> getHistoryTable() {
    return historyTable;
  }

  public void teardown() {
    primaryStage.close();
  }

  public AppController getController() {
    return controller;
  }

  public Label getMemoryUsageLabel() {

    return memoryUsageLabel;
  }
}
