package com.tom.cbm;

import javafx.collections.ObservableList;
import javafx.util.Duration;

/**
 * @author Thomas Povinelli
 * Created 2018-Feb-09
 * In ClipboardManager
 */
public class AppData {
  private final ObservableList<ClipboardTransaction> tableItems;
  private ClipboardTransaction lastTransaction = null;
  private Duration keyFrameDuration = Duration.seconds(1);
  private long maxMegabytes = 1024;

  public AppData(ObservableList<ClipboardTransaction> tableItems)
  {
    this.tableItems = tableItems;
  }

  public Duration getKeyFrameDuration() {
    return keyFrameDuration;
  }

  public void setKeyFrameDuration(Duration keyFrameDuration) {
    this.keyFrameDuration = keyFrameDuration;
  }

  public ObservableList<ClipboardTransaction> getTableItems() {
    return tableItems;
  }

  public ClipboardTransaction getLastTransaction() {
    return lastTransaction;
  }

  public void setLastTransaction(ClipboardTransaction lastTransaction) {
    this.lastTransaction = lastTransaction;
  }

  public long getMaxMegabytes() {
    return maxMegabytes;
  }

  public void setMaxMegabytes(long megs) {
    this.maxMegabytes = megs;
  }
}

