package com.tom.cbm;

import javafx.util.Duration;

import java.util.concurrent.atomic.AtomicBoolean;

public class ClipboardChecker extends Thread {

  private final AppController controller;
  private final AtomicBoolean done = new AtomicBoolean(false);
  private final AtomicBoolean holding = new AtomicBoolean(false);
  private Duration keyFrameDuration;

  public ClipboardChecker(Duration keyFrameDuration, AppController controller) {

    this.keyFrameDuration = keyFrameDuration;
    this.controller = controller;
    setDaemon(true);
  }

  @Override
  public void run() {
    while (!done.get()) {
      if (!holding.get()) {
        controller.updateDataAreaAction(false);
      }
      try {
        Thread.sleep((long) keyFrameDuration.toMillis());
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void changeDelay(Duration keyFrameDuration) {
    this.keyFrameDuration = keyFrameDuration;
    this.interrupt();
  }

  public AtomicBoolean getDone() {
    return done;
  }

  public void holdForMemory() {
    holding.getAndSet(true);
  }

  public void releaseGotMemory() {
    holding.getAndSet(false);
  }
}
