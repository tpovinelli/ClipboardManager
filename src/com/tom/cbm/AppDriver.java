package com.tom.cbm;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author Thomas Povinelli
 * Created 2018-Feb-09
 * In ClipboardManager
 */
public class AppDriver extends Application {

  private static AppGUI gui;

  public static void halt(int code) {
    gui.getController().getClipboardChecker().getDone().getAndSet(true);
    gui.getController().getClipboardChecker().interrupt();
    gui.teardown();
    // WHY does this crash the JVM ???
    // System.exit(code);
  }

  @Override
  public void start(Stage primaryStage) {
    gui = new AppGUI(primaryStage);
    gui.getPrimaryStage().show();
  }
}
