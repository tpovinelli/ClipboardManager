package com.tom.cbm;

import com.tom.io.pyle.FileMode;
import com.tom.io.pyle.Pyle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelReader;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;
import tom.utils.javafx.JavaFXUtilsKt;

import java.io.File;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static tom.utils.javafx.JavaFXUtilsKt.popupAlert;
import static tom.utils.javafx.JavaFXUtilsKt.promptForInputOptional;

/**
 * @author Thomas Povinelli
 * Created 2018-Feb-09
 * In ClipboardManager
 */
public class AppController {
  public static final UncaughtExceptionHandler oldHandler = Thread.getDefaultUncaughtExceptionHandler();
  public static final DataFormat[] DATA_FORMATS = new DataFormat[]{
      // the order of these is important and must be
      // this way. HTML, for example, always exists with
      // plain text so you have to check for HTML before
      // plain text to be able to retrieve HTML
      DataFormat.IMAGE, DataFormat.FILES,
      DataFormat.HTML, DataFormat.URL,
      DataFormat.RTF, DataFormat.PLAIN_TEXT
  };
  //  private final Timeline clipboardChecker;
  private final AppData appData;
  private final AppGUI appGUI;
  private final ClipboardChecker clipboardChecker;
  private int tableViewFontSize = 12;

  public AppController(@NotNull AppGUI appGUI) {
    this.appGUI = appGUI;
    appData = new AppData(appGUI.getHistoryTable().getItems());

    Thread.setDefaultUncaughtExceptionHandler(JavaFXUtilsKt::defaultWindowedExceptionHandler);

    clipboardChecker = new ClipboardChecker(appData.getKeyFrameDuration(), this);
    clipboardChecker.start();
  }

  public static void increaseFontSize(@NotNull Labeled labeled) {
    labeled.setFont(Font.font(labeled.getFont().getSize() * 1.25));
  }

  public static void decreaseFontSize(@NotNull Labeled labeled) {
    labeled.setFont(Font.font(labeled.getFont().getSize() * (1.0 / 1.25)));
  }

  public static int parseDuration(String string) {
    try {
      return Integer.parseInt(string);
    } catch (NumberFormatException e) {
      return -1;
    }
  }

  public ClipboardChecker getClipboardChecker() {
    return clipboardChecker;
  }

  public void updateDataAreaAction(boolean force) {
    Platform.runLater(() -> {
      Clipboard clipboard = Clipboard.getSystemClipboard();
      Optional<Pair<Object, DataFormat>> dataAndFormat = getDataAndFormat(clipboard);
      if (dataAndFormat.isPresent()) {
        Pair<Object, DataFormat> pair = dataAndFormat.get();
        Object content = pair.first;
        DataFormat dataFormat = pair.second;
        ClipboardTransaction lastTransaction = appData.getLastTransaction();
        if ((content != null && lastTransaction == null) || (force && lastTransaction == null)) {
          newClipboardTransaction(clipboard, content, dataFormat);
        } else if (content != null || force) {
          Object oldContent = lastTransaction.getData();
          if (!contentAreEqual(content, oldContent) || force) {
            newClipboardTransaction(clipboard, content, dataFormat);
          }
        }
        printMemoryReport();
      } else if (force) {
        Platform.runLater(() -> {
          String title = "Empty clipboard";
          String msg = "The clipboard is empty. Nothing to update";
          popupAlert(msg, title);
        });
      }
    });
  }

  private void newClipboardTransaction(Clipboard clipboard, Object content, DataFormat dataFormat) {
    ClipboardTransaction transaction;
    String plainString = getStringForContent(clipboard);
    transaction = new ClipboardTransaction(content, plainString, LocalDateTime.now(), dataFormat);

    appData.setLastTransaction(transaction);
    appData.getTableItems().add(transaction);
    memoryCheck();
  }

  @NotNull
  private String getStringForContent(Clipboard clipboard) {
    String plainString = clipboard.getString();
    if (plainString == null) {
      Set<DataFormat> contentTypes = clipboard.getContentTypes();
      for (DataFormat format : contentTypes) {
        if (clipboard.hasContent(format) && clipboard.getContent(format) != null) {
          plainString = clipboard.getContent(format).toString();
        }
      }
      if (plainString == null) {
        plainString = "<null>";
      }
    }
    return plainString;
  }

  private boolean contentAreEqual(Object content, Object oldContent) {
    if (content instanceof Image && oldContent instanceof Image) {
      return imagesEqualPixelByPixel((Image) content, (Image) oldContent);
    }
    return content.equals(oldContent);
  }

  private boolean imagesEqualPixelByPixel(Image image, Image oldImage) {
    int oldHeight = (int) oldImage.getHeight();
    int oldWidth = (int) oldImage.getWidth();
    int height = (int) image.getHeight();
    int width = (int) image.getWidth();
    if (oldHeight != height || oldWidth != width) {
      return false;
    }

    // RGBA (4 bytes) per pixel 1,000,000 bytes per megabyte
    // all times 2 because two images to compare
    // Avoids consuming excess memory for checking
    int oldImageSize = (oldHeight * oldWidth * 4) / 1_000_000;
    int newImageSize = (height * width * 4) / 1_000_000;
    if ((oldImageSize + newImageSize) > (appData.getMaxMegabytes() - usedMemoryMegabytes())) {
      return false;
    }

    // RGBA channels 1 byte per channel 4 channels per pixel
    byte[] oldPixels = new byte[(oldHeight * oldWidth * 4)];
    byte[] newPixels = new byte[(height * width * 4)];

    PixelReader oldReader = oldImage.getPixelReader();
    PixelReader newReader = image.getPixelReader();

    // Scan line stride is determined because 4 bytes per pixes in Bgra format
    oldReader.getPixels(0, 0, oldWidth, oldHeight, PixelFormat.getByteBgraInstance(), oldPixels, 0, oldWidth * 4);
    newReader.getPixels(0, 0, width, height, PixelFormat.getByteBgraInstance(), newPixels, 0, width * 4);

    return Arrays.equals(oldPixels, newPixels);
  }

  private Optional<Pair<Object, DataFormat>> getDataAndFormat(@NotNull Clipboard clipboard) {
    return Arrays.stream(DATA_FORMATS)
                 .filter(clipboard::hasContent)
                 .findFirst()
                 .map(d -> new Pair<>(clipboard.getContent(d), d));
  }

  public void registerCloseRequest() {
    appGUI.getPrimaryStage()
          .setOnCloseRequest(e -> {
            clipboardChecker.getDone().getAndSet(true);
            clipboardChecker.interrupt();
          });
  }

  public void clearDataAreaAction() {
    appData.getTableItems().clear();
  }

  public void clearClipboardAction() {
    appData.setLastTransaction(null);
    Clipboard.getSystemClipboard().clear();
  }

  public void copyItemAction() {
    Optional<ClipboardTransaction> selectedItem;
    selectedItem = Optional.ofNullable(appGUI.getHistoryTable()
                                             .getSelectionModel()
                                             .getSelectedItem());
    if (selectedItem.isPresent()) {
      ClipboardTransaction selection = selectedItem.get();
      DataFormat format = selection.getFormat();
      Object data = selection.getData();

      if (format == null || data == null) {
        return;
      }

      Clipboard.getSystemClipboard().setContent(Map.of(format, data));
    }
  }

  public void copyPlainItemAction() {
    Optional<ClipboardTransaction> selectedItem;
    selectedItem = Optional.ofNullable(appGUI.getHistoryTable()
                                             .getSelectionModel()
                                             .getSelectedItem());
    if (selectedItem.isPresent()) {
      ClipboardTransaction selection = selectedItem.get();
      Clipboard.getSystemClipboard().setContent(Map.of(DataFormat.PLAIN_TEXT, selection.getPlainData()));
    }

  }

  public void deleteItem() {
    Optional.ofNullable(appGUI.getHistoryTable()
                              .getSelectionModel()
                              .getSelectedItem())
            .ifPresent(it -> appData.getTableItems().remove(it));
  }

  public void increaseFontSizeActionHelper(@NotNull Parent p) {
    for (Node node : p.getChildrenUnmodifiable()) {
      if (node instanceof Labeled) {
        increaseFontSize((Labeled) node);
      } else if (node instanceof TableView) {
        TableView<?> table = (TableView) node;
        tableViewFontSize += 5;
        table.setStyle(String.format("-fx-font-size:%dpx;", tableViewFontSize));
      } else if (node instanceof Parent) {
        increaseFontSizeActionHelper((Parent) node);
      }
    }
  }

  public void decreaseFontSizeActionHelper(@NotNull Parent p) {
    for (Node node : p.getChildrenUnmodifiable()) {
      if (node instanceof Labeled) {
        decreaseFontSize((Labeled) node);
      } else if (node instanceof TableView) {
        TableView<?> table = (TableView) node;
        tableViewFontSize -= 5;
        table.setStyle(String.format("-fx-font-size:%dpx;", tableViewFontSize));
      } else if (node instanceof Parent) {
        decreaseFontSizeActionHelper((Parent) node);
      }
    }
  }

  public void increaseFontSizeAction() {
    increaseFontSizeActionHelper(appGUI.getPrimaryStage()
                                       .getScene()
                                       .getRoot());
  }

  public void decreaseFontSizeAction() {
    decreaseFontSizeActionHelper(appGUI.getPrimaryStage()
                                       .getScene()
                                       .getRoot());
  }

  public void changeRefreshDurationAction() {
    String msg = "Enter the number of seconds the program will\n" +
        "wait before checking to see if the system\n" +
        "clipboard has changed (1 <= value <= 3599)";

    String header = "Change clipboard refresh frequency";
    String title = "Change Refresh Rate";

    promptForInputOptional(title, header, msg)
        .ifPresent(r -> {
          int dur = parseDuration(r);
          if (dur < 1 || dur > 3599) {
            popupAlert("Clipboard refresh duration must be between 1 and 3599 incl.", "Invalid duration");
            return;
          }
          appData.setKeyFrameDuration(Duration.seconds(dur));
          updateKeyFrameDuration();
          popupAlert("Clipboard check duration set to " + dur + " seconds.", "Duration Set");
        });
  }

  private void updateKeyFrameDuration() {
    clipboardChecker.changeDelay(appData.getKeyFrameDuration());
  }

  public void keyPressedTableAction(@NotNull KeyEvent e) {
    if (e.getCode() == KeyCode.ESCAPE) {
      appGUI.getHistoryTable().getSelectionModel().clearSelection();
    } else if (e.getCode() == KeyCode.C || e.getCode() == KeyCode.X) {
      if (e.isShiftDown()) {
        copyPlainItemAction();
      } else {
        copyItemAction();
      }
    }
  }

  public void saveContent(Pyle pyle) {
    Optional<Pair<Object, DataFormat>> dataAndFormat = getDataAndFormat(Clipboard.getSystemClipboard());
    if (dataAndFormat.isPresent()) {
      Pair<Object, DataFormat> c = dataAndFormat.get();
      try {
        pyle.write(c.first.toString());
        pyle.close();
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
      Platform.runLater(() -> popupAlert("Successfully saved clipboard content to " + pyle.getAbsolutePath(), "Save successful"));
    } else {
      Platform.runLater(() -> popupAlert("Nothing to save ", "Not saved"));
    }
  }

  public void saveAsContentAction() {
    FileChooser fc = new FileChooser();
    File choice = fc.showSaveDialog(appGUI.getPrimaryStage());
    if (choice == null) {
      return;
    }
    try {
      Pyle pyle = new Pyle(choice.toURI(), FileMode.WRITE);
      saveContent(pyle);
      pyle.close();
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  public void saveContentAction() {
    try {
      Pyle pyle = new Pyle(System.getProperty("user.home") + File.separator + "clipboard.content", FileMode.WRITE);
      saveContent(pyle);
      pyle.close();
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  public void mouseClickedTableAction(@NotNull MouseEvent event) {
    if (event.getClickCount() == 2) {
      copyItemAction();
    }
  }

  public void changeMaxMemoryAction() {
    String msg =
        "Enter the max number of MEGABYTES that the program can use";
    String warning = "Bear in mind a couple of things: \n\t1) this is" +
        " not a hard limit. It is very likely that the program will overshoot\n" +
        "\tthis amount of memory before cleaning up and reclaiming some memory\n" +
        "\t2) Just because the JVM has freed some memory does not mean it will \n" +
        "\timmediately appear as freed in system process listing\n" +
        "\t3) This number will directly affect the amount of history the program\n" +
        "\t can store. If you set this low and nothing appears in the table, it is\n" +
        "\t likely that the clipboard item is too large to be stored in that amount\n" +
        "\t amount of memory. You should try increasing the amount of memory";

    String title = "Change Max Memory";

    Stage promptStage = new Stage();
    promptStage.setTitle(title);

    VBox root = new VBox();
    root.setAlignment(Pos.CENTER);
    root.setPadding(new Insets(10));
    root.setSpacing(10);

    Label messageLabel = new Label(msg);
    Font aDefault = Font.getDefault();
    messageLabel.setFont(Font.font(aDefault.getFamily(), FontWeight.BOLD, aDefault.getSize() * 1.2));
    Label warningLabel = new Label(warning);
    warningLabel.setLineSpacing(1.1);
    TextField field = new TextField();
    field.prefWidthProperty().unbind();
    field.setMaxWidth(450);

    HBox buttonBox = new HBox();
    buttonBox.setPadding(new Insets(5));
    buttonBox.setSpacing(5);
    Button yesButton = new Button("Save");
    Button cancelButton = new Button("Cancel");

    cancelButton.setOnAction(e -> promptStage.close());

    yesButton.setOnAction(e -> {
      int megs = parseDuration(field.getText());
      appData.setMaxMegabytes(megs);
      promptStage.close();
      memoryCheck();
      clipboardChecker.releaseGotMemory();
      popupAlert("Clipboard max memory set to " + megs + " megabytes.", "Memory Set");
    });

    yesButton.setPrefWidth(200);
    cancelButton.setPrefWidth(200);
    buttonBox.setAlignment(Pos.CENTER);
    buttonBox.getChildren().addAll(yesButton, cancelButton);
    buttonBox.setSpacing(5);
    buttonBox.setPadding(new Insets(5));

    root.getChildren().addAll(messageLabel, warningLabel, field, buttonBox);

    promptStage.setScene(new Scene(root));
    promptStage.showAndWait();
  }

  private long usedMemoryMegabytes() {
    Runtime runtime = Runtime.getRuntime();
    return (runtime.totalMemory() - runtime.freeMemory()) / 1000000;
  }

  private void memoryCheck() {
    printMemoryReport();

    // DO NOT CACHE SUBSEQUENT CALLS TO usedMemoryMegabytes() or appData.getMaxMegabytes()
    ObservableList<ClipboardTransaction> items = appGUI.getHistoryTable().getItems();
    int oldSize = items.size();
    int removed = 0;
    while (usedMemoryMegabytes() > appData.getMaxMegabytes() && !items.isEmpty()) {
      items.remove(0);
      removed++;
    }

    if (removed > 0) {
      System.gc();
    }

    if (removed == oldSize) {
      // force reupdating?
      appData.setLastTransaction(null);
    }

    if (usedMemoryMegabytes() > appData.getMaxMegabytes()) {
      clipboardChecker.holdForMemory();
      throw new OutOfMemoryError("Try increasing max app memory");
    } else if (removed > 0) {
      popupAlert("Removed " + removed + " items from history to free memory", "Items removed");
    }
  }

  void printMemoryReport() {
    long usedMemory = usedMemoryMegabytes();
    long totalMemory = appData.getMaxMegabytes();
    appGUI.getMemoryUsageLabel()
          .setText(String.format("Using %d MBs of %d MBs (%.2f%%)%n", usedMemory, totalMemory, ((double) usedMemory / totalMemory) * 100));
  }

  public AppData getAppData() {
    return appData;
  }
}
